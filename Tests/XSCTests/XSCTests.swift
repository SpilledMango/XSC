import XCTest
@testable import XSC

final class XSCTests: XCTestCase {
    func testHexStringsToUInt() {
        // Test some random strings
        XCTAssertEqual(UInt(fromHex: "0x1F38104375B"), 0x1F38104375B)
        XCTAssertEqual(UInt(fromHex: "1F38104375B"), 0x1F38104375B)
        XCTAssertEqual(UInt(fromHex: "0"), 0)
        XCTAssertEqual(UInt(fromHex: "0x0"), 0)
        XCTAssertEqual(UInt(fromHex: "0xFFFFFFFF"), 0xFFFFFFFF)
        XCTAssertEqual(UInt(fromHex: "FFFFFFFF"), 0xFFFFFFFF)
        XCTAssertEqual(UInt(fromHex: "0xDEADBEEFFED0FACE"), 0xDEADBEEFFED0FACE)
        XCTAssertEqual(UInt(fromHex: "DEADBEEFFED0FACE"), 0xDEADBEEFFED0FACE)
    }
    func testHexStringsToInt() {
        // Test some random strings
        XCTAssertEqual(Int(fromHex: "-0x1F38104375B"), -0x1F38104375B)
        XCTAssertEqual(Int(fromHex: "0x1F38104375B"), 0x1F38104375B)
        XCTAssertEqual(Int(fromHex: "0x0"), 0)
        XCTAssertEqual(Int(fromHex: "-0x0"), -0)
        XCTAssertEqual(Int(fromHex: "0xFFFFFFFF"), 0xFFFFFFFF)
        XCTAssertEqual(Int(fromHex: "-0xFFFFFFFF"), -0xFFFFFFFF)
        XCTAssertEqual(Int(fromHex: "-0xDEADBEEFFEDFACE"), -0xDEADBEEFFEDFACE)
        XCTAssertEqual(Int(fromHex: "0xDEADBEEFFEDFACE"), 0xDEADBEEFFEDFACE)
    }
    func testRandomStringsToInt() {
        for _ in 0..<4096 {
            var randomNumber = 0
            
            // A little quick and dirty, but easier...
            withUnsafeMutablePointer(to: &randomNumber) {
                ptr in
                arc4random_buf(ptr, 1)
            }
            
            let string1 = String(format: "%X", arguments: [randomNumber])
            let string2 = String(format: "0x%X", arguments: [randomNumber])
            
            XCTAssertEqual(Int(fromHex: string1), randomNumber)
            XCTAssertEqual(Int(fromHex: string2), randomNumber)
        }
    }
    func testRandomStringsToUInt() {
        for _ in 0..<4096 {
            let randomNumber = UInt(arc4random())
            
            let string1 = String(format: "%X", arguments: [randomNumber])
            let string2 = String(format: "0x%X", arguments: [randomNumber])
            
            XCTAssertEqual(UInt(fromHex: string1), randomNumber)
            XCTAssertEqual(UInt(fromHex: string2), randomNumber)
        }
    }

    static var allTests = [
        ("testHexStringsToUInt", testHexStringsToUInt),
        ("testRandomStringsToUInt", testRandomStringsToUInt),
        ("testHexStringsToInt", testHexStringsToInt)
    ]
}
