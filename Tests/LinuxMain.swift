import XCTest

import XSCTests

var tests = [XCTestCaseEntry]()
tests += XSCTests.allTests()
XCTMain(tests)