import Foundation

class ASTVarDecl: ASTDecl, CustomXMLConvertible {
    var key: Int
    
    var mutable: Bool
    var name: String
    
    var accessLevel: Accesslevel?
    
    var type: ASTTypeExpr
    
    init(name: String, accessLevel: Accesslevel, mutable: Bool, type: ASTTypeExpr) {
        self.name = name
        self.accessLevel = accessLevel
        self.mutable = mutable
        self.type = type
        
        self.key = getASTKey()
    }
    var xmlDescription: String {
        return "<ast-var-decl mutable=\"\(self.mutable)\"><ast-name>\(self.name)</ast-name>\(self.type.xmlDescription)</ast-var-decl>"
    }
}
