import Foundation

enum Accesslevel {
    case open
    case `public`
    case `internal`
    case `fileprivate`
    case `private`
}
