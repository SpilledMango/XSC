import Foundation

protocol ASTDecl: ASTBase {
    var name: String { get }
    var accessLevel: Accesslevel? { get }
}
