import Foundation

class ASTVarAssignStat: ASTStat {
    var key: Int
    
    
    var lvalue: ASTVarDecl
    var rvalue: ASTExpr
    
    init(lvalue: ASTVarDecl, rvalue: ASTExpr) {
        self.lvalue = lvalue
        self.rvalue = rvalue
        
        self.key = getASTKey()
    }
    
    var xmlDescription: String {
        return "<ast-var-assign-stat><ast-lvalue><ast-ref key=\"\(self.lvalue.key)\" /></ast-lvalue><ast-rvalue><ast-ref key=\"\(self.rvalue.key)\" /></ast-rvalue></ast-var-assign-stat>"
    }
}
