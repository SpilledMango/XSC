import Foundation

protocol ASTLiteralExpr: ASTExpr {
    associatedtype LiteralType
    var literalValue: LiteralType { get }
    init(literal: LiteralType)
}
