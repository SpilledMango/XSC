import Foundation

class ASTNumLiteralExpr<LiteralType>: ASTLiteralExpr {
    var key: Int
    
    var literalValue: LiteralType
    
    required init(literal value: LiteralType) {
        self.literalValue = value
        self.key = getASTKey()
    }
    var xmlDescription: String {
        let typeString: String
        
        switch LiteralType.self {
        case is Int64.Type:
            typeString = "signed-integer"
        case is UInt64.Type:
            typeString = "unsigned-integer"
        case is Double.Type:
            typeString = "float"
        default:
            typeString = "unknown"
        }
        return "<ast-\(typeString)-literal-expr>\(self.literalValue)</ast-\(typeString)-literal-expr>"
    }
}
