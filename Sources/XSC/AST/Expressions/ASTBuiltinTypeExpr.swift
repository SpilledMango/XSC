import Foundation

protocol ASTTypeExpr: ASTExpr {
}
class ASTBuiltinTypeExpr: ASTTypeExpr, CustomXMLConvertible {
    var key: Int
    
    
    enum Types: String, CustomStringConvertible {
        var description: String {
            return self.rawValue
        }
        
        case bool = "Bool"
        case int8 = "Int8", int16 = "Int16", int32 = "Int32", int = "Int", int64 = "Int64"
        case uint8 = "UInt8", uint16 = "UInt16", uint32 = "UInt32", uint = "UInt", uint64 = "UInt64"
        case float = "Float", double = "Double"
        
        // TODO Maybe strings, characters, optionals here as well?
    }
    
    let type: ASTBuiltinTypeExpr.Types
    
    init(type: ASTBuiltinTypeExpr.Types) {
        self.type = type
        self.key = getASTKey()
    }
    
    var name: String {
        return self.type.description
    }
    var accessLevel: Accesslevel? {
        return Accesslevel.open
    }
    var xmlDescription: String {
        return "<ast-type-builtin>\(self.name)</ast-type-builtin>"
    }
}
