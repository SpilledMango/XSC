import Foundation

protocol CustomXMLConvertible: CustomStringConvertible {
    var xmlDescription: String { get }
}
extension CustomXMLConvertible {
    var description: String {
        return self.xmlDescription
    }
}
