import Foundation

protocol ASTBase: CustomXMLConvertible {
    var key: Int { get }
}

var currentASTMaxKey = -1

func getASTKey() -> Int {
    currentASTMaxKey += 1
    return currentASTMaxKey
}
