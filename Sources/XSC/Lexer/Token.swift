import Foundation

// MARK: - Tokens
enum Token {
    // MARK: - Basic Tokens
    case identifier(String)
    case numberLiteral(String), stringLiteral(String)
    
    // MARK: - Parentheses
    case paranthesisOpen, paranthesisClose
    case squareBracketOpen, squareBracketClose
    case curlyBracketOpen, curlyBracketClose
    
    // MARK: - Special characters
    case dot, comma, colon, semicolon
    
    case equalSign
    
    case exclamationMark, questionMark
    
    case plusSign, minusSign
    case asterisk
    case slash
    case percent
    
    case tilde
    case caret
    case ampersand
    case verticalBar
    
    case greaterThan, lessThan
    
    // MARK: - Declarative Keywords
    case `let`, variable
    case function, `operator`
    case `class`, structure, enumeration, `protocol`
    case initializer, deinitialzer
    case `extension`
    
    case associatedType
    case typeAlias
    
    case `import`
    
    case `subscript`
    
    case `fileprivate`, `private`, `internal`, `public`, `open`
    case `static`
    case `inout`
    
    
    // MARK: - Keywords used in statements
    case `break`, `continue`, `return`, `fallthrough`, `throw`
    case `case`, `default`
    case `defer`
    case `do`
    case `repeat`, `while`
    case `if`, `else`, `guard`
    case `for`, `in`
    case `switch`, `where`
    
    // MARK: - Keywords used in expressions and types
    case `as`, `is`
    case anyType
    case `try`, `catch`
    case `true`, `false`, `nil`
    case `throws`, `rethrows`
    case `self`, `super`
    case selfType
    
    // MARK: - Keywords used in patterns
    case underscore
    
    // MARK: - Keywords used in particular contexts
    case associativity
    case convenience
    case dynamic
    case willSet, didSet
    case get, set
    case final
    case required
    
    case prefix, postfix, infix
    case indirect
    case lazy
    case mutating, nonmutating
    case left, right
    case optional
    case override
    case precedence
    
    case protocolType
    case typeType
    
    case unowned, weak
    
    // MARK: - Preprocessor stuff...
    case ppAvailable
    case ppColumn
    case ppElse
    case ppElseif
    case ppEndif
    case ppFile
    case ppFunction
    case ppFf
    case ppImageLiteral, ppFileLiteral, ppColorLiteral
    case ppLine
    case ppSelector
    case ppSourceLocation
    
    // MARK: - Misc
    case eof
    
    func isEOF() -> Bool {
        if case Token.eof = self {
            return true
        }
        return false
    }
}
