import Foundation

class Lexer {
    private var _tokens = [Token]()
    var tokens: [Token] {
        return _tokens
    }
    
    private var url: URL?
    private let fileString: String
    
    private var character: Character!
    private var line = 1
    
    private var word = ""
    private var stringLiteral = ""
    private var currentIndex: String.Index
    private var isInsideStringLiteral = false
    
    private var paranthesisLevel = 0
    private var squareBracketLevel = 0
    private var curlyBracketLevel = 0
    
    private var commentLevel = 0
    
    private var makingNumber = false
    private var makingFloat = false
    private var numberLiteral = ""
    
    private var singleLineComment = false
    
    private var previousCharacter: Character!
    private var previousSeparator: Character!
    
    init(fileString: String) {
        self.fileString = fileString
        self.currentIndex = fileString.startIndex
    }
    
    convenience init(url: URL) {
        do {
            self.init(fileString: try String(contentsOf: url))
        } catch {
            print(error)
            exit(EXIT_FAILURE)
        }
        self.url = url
    }
    private func syntaxError(_ error: String) {
        if let url = self.url {
            print("At \(url.absoluteString):\(line)")
        }
        print("Syntax error: \(error)")
        exit(EXIT_FAILURE)
    }
    private func token(from word: String) -> Token {
        if word.count > 2 && word.first == "`" && word.last == "`" {
            return Token.identifier(String(word[word.index(after: word.startIndex)..<word.index(before: word.endIndex)]))
        }
        if previousSeparator == "." {
            return Token.identifier(word)
        }
        switch word {
        case "let":
            return Token.let
        case "var":
            return Token.variable
        case "func":
            return Token.function
        case "class":
            return Token.class
        case "struct":
            return Token.structure
        case "enum":
            return Token.enumeration
        case "protocol":
            return Token.protocol
        case "init":
            return Token.initializer
        case "deinit":
            return Token.deinitialzer
        case "associatedtype":
            return Token.associatedType
        case "typealias":
            return Token.typeAlias
        case "import":
            return Token.import
        case "extension":
            return Token.extension
        case "private":
            return Token.private
        case "fileprivate":
            return Token.fileprivate
        case "internal":
            return Token.internal
        case "public":
            return Token.public
        case "open":
            return Token.open
        case "subscript":
            return Token.subscript
        case "case":
            return Token.case
        case "inout":
            return Token.inout
        
        case "if":
            return Token.if
        case "else":
            return Token.else
        case "guard":
            return Token.guard
        case "switch":
            return Token.switch
            
        case "for":
            return Token.for
        case "in":
            return Token.in
            
        case "while":
            return Token.while
        case "repeat":
            return Token.repeat
            
        case "break":
            return Token.break
        case "continue":
            return Token.continue
        case "fallthrough":
            return Token.fallthrough
        case "return":
            return Token.return
        case "throw":
            return Token.throw
            
        case "as":
            return Token.as
        case "is":
            return Token.is
            
        case "_":
            return Token.underscore
        default:
            return Token.identifier(word)
        }
    }
    private func continueMakingNumber(with character: Character) -> Bool {
        switch character {
        case "0"..."9", "a"..."f", "A"..."F", "b", "x", "o":
            numberLiteral.append(character)
        case ".":
            self.makingFloat = true
            numberLiteral.append(character)
        default:
            self.makingNumber = false
            self.makingFloat = false
            return false
        }
        return true
    }
    private func processCommented(_ character: Character) {
        if character == "\n" {
            singleLineComment = false
            previousCharacter = nil
        }
    }
    private func processMultilineComment(_ character: Character) {
        if character == "/" && previousCharacter == "*" {
            commentLevel -= 1
        }
    }
    private func separate() -> [Token] {
        if self.word.isEmpty {
            if self.numberLiteral.isEmpty {
                return []
            }
            let numberLiteral = self.numberLiteral
            self.numberLiteral = ""
            return [Token.numberLiteral(numberLiteral)]
            
        }
        let word = self.word
        self.word = ""
        return [token(from: word)]
    }
    private func token(fromSign sign: Character) -> Token {
        previousSeparator = sign
        switch sign {
        case ".":
            return .dot
        case ",":
            return .comma
        case ":":
            return .colon
        case "=":
            return .equalSign
        case "+":
            return .plusSign
        case "-":
            return .minusSign
        case "%":
            return .percent
        case "~":
            return .tilde
        case "^":
            return .caret
        case "&":
            return .ampersand
        case "|":
            return .verticalBar
        case "<":
            return .lessThan
        case ">":
            return .greaterThan
        case "!":
            return .exclamationMark
        case "?":
            return .questionMark
        case ";":
            return .semicolon
        default:
            fatalError("Not Swift special character: \(sign)")
        }
    }
    private func specialSign(_ character: Character) -> [Token] {
        return separate() + [token(fromSign: character)]
    }
    private func getTokens() -> [Token] {
        
        let substring = fileString[currentIndex...]
        for index in substring.indices {
            currentIndex = substring.index(after: currentIndex)
            
            character = fileString[index]
            var letter: Character?
            
            if commentLevel > 0 {
                processMultilineComment(character)
                previousCharacter = character
                continue
            }
            
            if singleLineComment {
                processCommented(character)
                continue
            }
            
            if character == "\"" {
                if isInsideStringLiteral {
                    
                    if previousCharacter == "\\" {
                        self.stringLiteral.append(character)
                        previousCharacter = character
                        continue
                    }
                    
                    isInsideStringLiteral = false
                    let stringLiteral = self.stringLiteral
                    self.stringLiteral = ""
                    return [Token.stringLiteral(stringLiteral)]
                }
                isInsideStringLiteral = true
                continue
            }
            
            if isInsideStringLiteral {
                if character == "\\" {
                    previousCharacter = character
                    continue
                }
                self.stringLiteral.append(character)
                previousCharacter = character
                continue;
            }
            
            if makingNumber {
                if continueMakingNumber(with: character) {
                    continue
                }
            }
            
            switch character {
            case " ", "\n", "\r", "\u{9}", "\u{11}", "\u{12}", "\0":
                if !word.isEmpty || !numberLiteral.isEmpty {
                    return separate()
                }
                if character == "\n" {
                    line += 1
                    singleLineComment = false
                }
                continue
            case "(":
                paranthesisLevel += 1
                return separate() + [.paranthesisOpen]
            case ")":
                paranthesisLevel -= 1
                if paranthesisLevel < 0 {
                    syntaxError("One extra ).")
                }
                return separate() + [.paranthesisClose]
            case "[":
                squareBracketLevel += 1
                return separate() + [.squareBracketOpen]
            case "]":
                squareBracketLevel -= 1
                if squareBracketLevel < 0 {
                    syntaxError("One extra ].")
                }
                return separate() + [.squareBracketClose]
            case "{":
                // TODO maybe separate() as well?
                curlyBracketLevel += 1
                return [Token.curlyBracketOpen]
            case "}":
                curlyBracketLevel -= 1
                if curlyBracketLevel < 0 {
                    syntaxError("One extra }.")
                }
                return [Token.curlyBracketClose]
            case "/":
                if previousCharacter == "/" {
                    self.singleLineComment = true
                    continue;
                }
            case "*":
                if previousCharacter == "/" {
                    commentLevel += 1
                }
            case ".", ",", ":", ";", "=",
                 "+", "-",
                 "~", "^", "&", "|",
                 "<", ">",
                 "!", "?":
                return specialSign(character)
            
            case _ where character.isLetter || character == "`" || character == "_":
                letter = character
            case _ where character.isDigit:
                if word.isEmpty && numberLiteral.isEmpty  {
                    makingNumber = true
                    numberLiteral.append(character)
                    continue
                }
                if !makingNumber {
                    letter = character
                }
            case "\\":
                syntaxError("Backslash not allowed outside literal.")
            default:
                break;
            }
            if let letter = letter {
                word.append(letter)
            }
            previousCharacter = character
        }
        return [.eof]
    }
    func parse() {
        loop: while true {
            let tokens = self.getTokens()
            for token in tokens {
                switch token {
                case .eof:
                    break loop
                default: self._tokens.append(token)
                }
            }
        }
    }
}
