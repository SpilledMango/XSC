import Foundation

extension Character {
    func isCompletelyAMemberOf(characterSet: CharacterSet) -> Bool {
        for scalar in self.unicodeScalars {
            guard characterSet.contains(scalar) else {
                return false
            }
        }
        return true
    }
    var isLetter: Bool {
        return self.isCompletelyAMemberOf(characterSet: CharacterSet.letters)
    }
    var isDigit: Bool {
        return self.isCompletelyAMemberOf(characterSet: CharacterSet.decimalDigits)
    }
    var utf32Value: UInt32? {
        guard self.unicodeScalars.count == 1 else {
            return nil
        }
        return self.unicodeScalars.first!.value
    }
}
extension UnsignedInteger {
    init?(_ digit: Character, base: Int) {
        guard let digitValue = String(digit).lowercased().first?.utf32Value else {
            // We don't want emojis.
            return nil
        }
        let zero = Character("0").utf32Value!
        let nine = Character("9").utf32Value!
        
        if base <= 10 {
        
            guard zero...nine ~= digitValue else {
                return nil
            }
            self = Self(digitValue - zero)
        }
        let a = Character("a").utf32Value!
        let f = Character("f").utf32Value!
        
        if zero...nine ~= digitValue {
            self = Self(digitValue - zero)
        } else if a...f ~= digitValue {
            self = Self(10 + digitValue - a)
        } else {
            return nil
        }
    }
}
public protocol IntegerWithMinMax {
    static var max: Self { get }
    static var min: Self { get }
}

extension Int8: IntegerWithMinMax {}
extension Int16: IntegerWithMinMax {}
extension Int32: IntegerWithMinMax {}
extension Int: IntegerWithMinMax {}
extension Int64: IntegerWithMinMax {}

extension UInt8: IntegerWithMinMax {}
extension UInt16: IntegerWithMinMax {}
extension UInt32: IntegerWithMinMax {}
extension UInt: IntegerWithMinMax {}
extension UInt64: IntegerWithMinMax {}

extension UnsignedInteger {
    public init?(fromHex _string: String) {
        
        var string = _string
        
        if string.hasPrefix("0x") {
            string = String(_string[string.index(after: string.index(of: "x")!)...])
        }
        
        if string.hasPrefix("-") {
            return nil
        }
        
        var number: Self = 0
        var index: Self = 0
        var power: Self = 1
        
        for character in string.reversed() {
            guard let digit = Self(character, base: 16) else {
                return nil
            }
            number += digit * power
            index += 1
            
            guard index < 16 else {
                continue
            }
            
            power *= 16
        }
    
        self = number
    }
}
extension SignedInteger where Self: IntegerWithMinMax {
    public init?(fromHex _string: String) {
        var string = _string
        
        var positive = true
        
        if string.hasPrefix("-") {
            positive = false
            string = String(string[string.index(after: string.index(of: "-")!)...])
        }
        
        string = string.lowercased()
        
        guard let absoluteValue = UInt64(fromHex: string) else {
            return nil
        }
        guard absoluteValue <= UInt64(Self.max) else {
            return nil
        }
        if positive {
            self = +Self(absoluteValue)
        } else {
            self = -Self(absoluteValue)
        }
    }
}
func parseNumberLiteral(literal: String) {
    if literal.hasPrefix("0x") {
        
    }
}
