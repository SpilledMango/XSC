import Foundation
import CommandCougar

class Compiler {
    private var evaluation: CommandEvaluation
    private var urls: [URL]
    
    weak var parser: Parser!
    var errorReporter: ErrorReporter
    
    var dumpTokens: Bool
    
    var errors: [CompilerError]
    
    init(evaluation: CommandEvaluation) {
        self.evaluation = evaluation
        
        self.dumpTokens = self.evaluation.options.contains(XSCOptions.dumpTokens)
        
        self.urls = self.evaluation.parameters.map {
            URL(fileURLWithPath: $0)
        }
        self.errorReporter = ErrorReporter()
        self.errors = []
        
        self.errorReporter.compiler = self
    }
    func compile() {
        for url in self.urls {
            self.compile(url: url)
        }
    }
    func compile(url: URL) {
        guard FileManager.default.fileExists(atPath: url.path) else {
            print("Cannot compile '\(url.absoluteString)': no such file or directory.")
            exit(EXIT_FAILURE)
        }
        
        let parser = Parser(compiler: self, url: url)
        
        if self.dumpTokens {
            _ = parser.lexer.tokens.map {
                print($0)
            }
            exit(EXIT_FAILURE)
        }
        
        print("Compiling '\(url.absoluteString)'")
        parser.parseToTree()
        
        for item in parser.tree.items {
            print(item.xmlDescription)
        }
        
        for error in self.errors {
            print(error.xmlDescription)
        }
        
        print("Compiled '\(url.absoluteString)'")
    }
}
