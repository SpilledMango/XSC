import Foundation
import CommandCougar

enum XSCOptions {
    static let outputs      = Option(flag: .both(short: "o", long: "output"), overview: "Set output file", parameterName: "output")
    static let dumpTokens   = Option(flag: .long("dump-tokens"), overview: "Only run the lexer, and print tokens.")
}

let options = [
    XSCOptions.outputs,
    XSCOptions.dumpTokens,
]
let parameters = [Parameter.required("input files")]

func callback(_ evaluation: CommandEvaluation) {
    let compiler = Compiler(evaluation: evaluation)
    compiler.compile()
}

let cli = Command(name: "XSC", overview: "Xotik Swift Compiler", callback: callback, options: options, parameters: parameters)

do {
    let evaluation = try cli.evaluate(arguments: CommandLine.arguments)
    try evaluation.performCallbacks()
} catch {
    print("Fatal error: \(error).")
    exit(EXIT_FAILURE)
}
