import Foundation

protocol CompilerError: CustomXMLConvertible {}

enum ParserError: CompilerError {
    case wrongToken(Token, correctTokens: [Token])
    case unknownIdentifier(String)
    
    var xmlDescription: String {
        switch self {
        case ParserError.wrongToken(let token, correctTokens: let correctTokens):
            var correctTokensXML = ""
            
            for token in correctTokens {
                correctTokensXML += "<token>\(token)</token>"
            }
            
            return "<parser-error-wrong-token><wrong-token>\(token)</wrong-token><correct-tokens>\(correctTokensXML)</correct-tokens></parser-error-wrong-token>"
        case ParserError.unknownIdentifier(let identifier):
            return "<parser-error-unknown-identifier>\(identifier)</parser-error-unknown-identifier>"
//        default:
//            return "<parser-error-unknown />"
        }
    }
}
