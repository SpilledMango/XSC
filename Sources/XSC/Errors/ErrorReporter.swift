import Foundation

class ErrorReporter {
    weak var compiler: Compiler!
    
    /// Report an error by adding it to the Compiler's error list.
    func report(error: CompilerError) {
        self.compiler.errors.append(error)
    }
    
    func report(unknownIdentifier identifier: String) {
        self.report(error: ParserError.unknownIdentifier(identifier))
    }
    
    func report(expected: [Token], found: Token) {
        self.report(error: ParserError.wrongToken(found, correctTokens: expected))
    }
    
    func lexerError(_ message: String) -> Never {
        print("Lexer error: \(message).")
        exit(EXIT_FAILURE)
    }
    func parserError(_ message: String) -> Never {
        print("Parser error: \(message).")
        exit(EXIT_FAILURE)
    }
    func internalError(_ message: String) -> Never {
        print("Internal error: \(message).")
        exit(EXIT_FAILURE)
    }
}
