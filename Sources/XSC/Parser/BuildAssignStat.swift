import Foundation

extension Parser {
    func buildAssignStat() {
        // Get the lvalue (decl).
        let lvalueToken = self.getToken()
        guard case Token.identifier(let identifierName) = lvalueToken else {
            self.compiler.errorReporter.internalError("Function \(#function) expects only identifier tokens first")
        }
        guard let lvalue = self.getASTDecl(from: identifierName) else {
            self.compiler.errorReporter.report(unknownIdentifier: identifierName)
            self.advance(steps: 2)
            return
        }
        
        // Get the equal sign.
        // TODO +=, -=, *=, /=, ^= etc.
        let equalSignToken = self.getToken()
        guard case Token.equalSign = equalSignToken else {
            self.compiler.errorReporter.report(expected: [Token.equalSign], found: equalSignToken)
            return
        }
        
        // Get the rvalue (expr).
        // TODO Wrap getting this expression into a function,
        // thus supporting expressions containing more than one token,
        // e.g. 2 + 1, (23 - 1) * 2 etc.
        let rvalueToken = self.getToken()
        guard case Token.numberLiteral(let literalString) = rvalueToken, let number = Int(literalString) else {
            self.compiler.errorReporter.report(expected: [Token.numberLiteral("(any)")], found: rvalueToken)
            return
        }
        let rvalue = ASTNumLiteralExpr<Int>(literal: number)
        
        if let lvalueVarDecl = lvalue as? ASTVarDecl {
            let stat = ASTVarAssignStat(lvalue: lvalueVarDecl, rvalue: rvalue)
            self.tree.items.append(stat)
        }
        endStatement()
    }
}
