import Foundation

extension Parser {
    func buildASTVarAssignStat(lvalue: ASTVarDecl) {
        let token = self.getToken()
        guard case Token.equalSign = token else {
            self.compiler.errorReporter.report(expected: [Token.equalSign], found: token)
            return
        }
        let rvalueToken = self.getToken()
        
        switch rvalueToken {
        case Token.numberLiteral(let numberLiteral):
            print(numberLiteral)
            let rvalue = ASTNumLiteralExpr<Int>(literal: Int(numberLiteral)!)
            let astItem = ASTVarAssignStat(lvalue: lvalue, rvalue: rvalue)
            self.tree.items.append(astItem)
        default:
            self.compiler.errorReporter.report(expected: [Token.numberLiteral("(any)")], found: rvalueToken)
        }
    }
    func buildASTVarDecl() {
        let mutable: Bool
        var astItem: ASTVarDecl!
        
        let token = self.getToken()
        
        switch token {
        case Token.variable:
            mutable = true
        case Token.let:
            mutable = false
        default:
            self.compiler.errorReporter.report(expected: [Token.variable, Token.let], found: token)
            return
        }
        
        let name = self.assumeIdentifier(for: self.getToken()) {
            wrongToken in
            self.compiler.errorReporter.report(expected: [Token.identifier("(any)")], found: wrongToken)
        }
        
        func continueWithExplicitType() -> Bool {
            self.advance(steps: 1)
            let typeNameToken = self.getToken()
            guard case Token.identifier(let typeName) = typeNameToken else {
                return false
            }
            guard let type = ASTBuiltinTypeExpr.Types(rawValue: typeName) else {
                return false
            }
            astItem = ASTVarDecl(name: name, accessLevel: Accesslevel.open, mutable: mutable, type: ASTBuiltinTypeExpr(type: type))
            self.tree.items.append(astItem)
            return true
        }
        func continueWithInferredType() -> Bool {
            return true
        }
        
        switch self.getToken(advance: false) {
        case Token.colon:
            guard continueWithExplicitType() else {
                return
            }
        default:
            break
        }
        
        switch self.getToken(advance: false) {
        case Token.equalSign:
            guard let astItem = astItem else {
                fatalError("Implicit typing not supported yet.")
            }
            self.buildASTVarAssignStat(lvalue: astItem)
        default:
            break
        }
        
        endStatement()
    }
}
