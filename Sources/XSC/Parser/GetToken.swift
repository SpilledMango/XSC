import Foundation

extension Parser {
    func advance(steps: Int) {
        self.currentIndex += steps
    }
    
    func getToken(advance: Bool = true) -> Token {
        guard self.lexer.tokens.indices.contains(self.currentIndex) else {
            return Token.eof
        }
        
        let token = self.lexer.tokens[self.currentIndex]
        if advance {
            self.advance(steps: 1)
        }
        return token
    }
    func getTokens(count: Int, advance: Bool = false) -> [Token] {
        fatalError()
    }
}
