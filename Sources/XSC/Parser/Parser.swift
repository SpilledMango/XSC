import Foundation

class Parser {
    var compiler: Compiler
    var lexer: Lexer
    var tree: AST
    
    var accessLevel: Accesslevel?
    
    init(compiler: Compiler, url: URL) {
        self.lexer = Lexer(url: url)
        self.lexer.parse()
        self.currentIndex = self.lexer.tokens.startIndex
        self.tree = AST()
        self.compiler = compiler
    }
    
    var currentIndex: Array<Token>.Index
    
    func assumeIdentifier(for token: Token, or body: @escaping (Token) throws -> Void) rethrows -> String {
        switch token {
        case .identifier(let name):
            return name
        default:
            try body(token)
            return ""
        }
    }
    func endStatement() {
        let nextOptionalToken = self.getToken(advance: false)
        if case Token.semicolon = nextOptionalToken {
            self.advance(steps: 1)
            return
        }
    }
    private func getType(from string: String) {
        
    }
    
    func getASTDecl(from identifier: String) -> ASTDecl? {
        let decl: ASTVarDecl? = nil
        
        for astItem in self.tree.items {
            if let astVarDecl = astItem as? ASTVarDecl {
                if astVarDecl.name == identifier {
                    return astVarDecl
                }
            }
        }
        
        return decl
    }
    private func clear() {
        self.accessLevel = nil
    }
    func parseToTree() {
        var token = Token.eof
        repeat {
            token = self.getToken(advance: false)
            self.clear()
            
            switch token {
            case Token.let, Token.variable:
                self.buildASTVarDecl()
            case Token.identifier(_):
                self.buildAssignStat()
            default:
                break;
            }
        } while !token.isEOF()
    }
}
